"use strict";
var media_metadata_retriever_common_1 = require("./media-metadata-retriever.common");
var MediaMetadataRetriever = (function (_super) {
    __extends(MediaMetadataRetriever, _super);
    function MediaMetadataRetriever() {
        var _this = _super.call(this) || this;
        _this._mediaMetadataRetriever = new android.media.MediaMetadataRetriever();
        console.log("[nativescript-media-metadata-retriever]: MediaMetadataRetriever object created...");
        return _this;
    }
    MediaMetadataRetriever.prototype.extractAllMetadata = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                resolve({
                    album: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_ALBUM),
                    albumartist: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_ALBUMARTIST),
                    artist: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_ARTIST),
                    author: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_AUTHOR),
                    bitrate: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_BITRATE),
                    cdtracknumber: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_CD_TRACK_NUMBER),
                    compilation: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_COMPILATION),
                    composer: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_COMPOSER),
                    date: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_DATE),
                    disknumber: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_DISK_NUMBER),
                    duration: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_DURATION),
                    genre: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_GENRE),
                    hasaudio: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_HAS_AUDIO),
                    haslocation: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_HAS_LOCATION),
                    hasmimetype: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_HAS_MIMETYPE),
                    hasvideo: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_HAS_VIDEO),
                    numtracks: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_NUM_TRACKS),
                    title: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_TITLE),
                    videoheight: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_VIDEO_HEIGHT),
                    videorotation: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_VIDEO_ROTATION),
                    width: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_VIDEO_WIDTH),
                    write: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_WRITE),
                    year: _this._mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever._METADATA_KEY_YEAR)
                });
                console.log("[nativescript-media-metadata-retriever]: All metadata extracted...");
            }
            catch (ex) {
                console.log("[nativescript-media-metadata-retriever]: Failed to extract all metadata...");
                reject(ex);
            }
        });
    };
    MediaMetadataRetriever.prototype.extractMetadata = function (keyCode) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                if (keyCode) {
                    resolve(_this._mediaMetadataRetriever.extractMetadata(keyCode));
                    console.log("[nativescript-media-metadata-retriever]: Metadata extracted...");
                }
                else {
                    console.log("[nativescript-media-metadata-retriever]: keyCode is required...");
                    reject("[nativescript-media-metadata-retriever]: keyCode is required...");
                }
            }
            catch (ex) {
                console.log("[nativescript-media-metadata-retriever]: Failed to extract metadata...");
                reject(ex);
            }
        });
    };
    MediaMetadataRetriever.prototype.getEmbeddedPicture = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                var bitmap = android.graphics.BitmapFactory.decodeByteArray(_this._mediaMetadataRetriever.getEmbeddedPicture(), 0, _this._mediaMetadataRetriever.getEmbeddedPicture().length);
                resolve(bitmap);
                console.log("[nativescript-media-metadata-retriever]: Bitmap generated...");
            }
            catch (ex) {
                console.log("[nativescript-media-metadata-retriever]: Failed to generate bitmap...");
                reject(ex);
            }
        });
    };
    MediaMetadataRetriever.prototype.setDataSource = function (path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                if (path) {
                    var _file = new java.io.File(path + '');
                    var _fis = new java.io.FileInputStream(_file.getAbsolutePath());
                    console.log("[nativescript-media-metadata-retriever]: Setting data source to - " + _file.getAbsolutePath());
                    _this._mediaMetadataRetriever.setDataSource(_fis.getFD());
                    console.log("[nativescript-media-metadata-retriever]: Data source has been set...");
                }
                else {
                    console.log("[nativescript-media-metadata-retriever]: Failed to set data source...");
                    reject('[nativescript-media-metadata-retriever]: path is required');
                }
            }
            catch (ex) {
                console.log("[nativescript-media-metadata-retriever]: Failed to set data source...");
                reject(ex);
            }
        });
    };
    return MediaMetadataRetriever;
}(media_metadata_retriever_common_1.Common));
MediaMetadataRetriever._METADATA_KEY_ALBUM = android.media.MediaMetadataRetriever.METADATA_KEY_ALBUM;
MediaMetadataRetriever._METADATA_KEY_ALBUMARTIST = android.media.MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST;
MediaMetadataRetriever._METADATA_KEY_ARTIST = android.media.MediaMetadataRetriever.METADATA_KEY_ARTIST;
MediaMetadataRetriever._METADATA_KEY_AUTHOR = android.media.MediaMetadataRetriever.METADATA_KEY_AUTHOR;
MediaMetadataRetriever._METADATA_KEY_BITRATE = android.media.MediaMetadataRetriever.METADATA_KEY_BITRATE;
MediaMetadataRetriever._METADATA_KEY_CD_TRACK_NUMBER = android.media.MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER;
MediaMetadataRetriever._METADATA_KEY_COMPILATION = android.media.MediaMetadataRetriever.METADATA_KEY_COMPILATION;
MediaMetadataRetriever._METADATA_KEY_COMPOSER = android.media.MediaMetadataRetriever.METADATA_KEY_COMPOSER;
MediaMetadataRetriever._METADATA_KEY_DATE = android.media.MediaMetadataRetriever.METADATA_KEY_DATE;
MediaMetadataRetriever._METADATA_KEY_DISK_NUMBER = android.media.MediaMetadataRetriever.METADATA_KEY_DISC_NUMBER;
MediaMetadataRetriever._METADATA_KEY_DURATION = android.media.MediaMetadataRetriever.METADATA_KEY_DURATION;
MediaMetadataRetriever._METADATA_KEY_GENRE = android.media.MediaMetadataRetriever.METADATA_KEY_GENRE;
MediaMetadataRetriever._METADATA_KEY_HAS_AUDIO = android.media.MediaMetadataRetriever.METADATA_KEY_HAS_AUDIO;
MediaMetadataRetriever._METADATA_KEY_HAS_VIDEO = android.media.MediaMetadataRetriever.METADATA_KEY_HAS_VIDEO;
MediaMetadataRetriever._METADATA_KEY_HAS_LOCATION = android.media.MediaMetadataRetriever.METADATA_KEY_LOCATION;
MediaMetadataRetriever._METADATA_KEY_HAS_MIMETYPE = android.media.MediaMetadataRetriever.METADATA_KEY_MIMETYPE;
MediaMetadataRetriever._METADATA_KEY_NUM_TRACKS = android.media.MediaMetadataRetriever.METADATA_KEY_NUM_TRACKS;
MediaMetadataRetriever._METADATA_KEY_TITLE = android.media.MediaMetadataRetriever.METADATA_KEY_TITLE;
MediaMetadataRetriever._METADATA_KEY_VIDEO_HEIGHT = android.media.MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT;
MediaMetadataRetriever._METADATA_KEY_VIDEO_ROTATION = android.media.MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION;
MediaMetadataRetriever._METADATA_KEY_VIDEO_WIDTH = android.media.MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH;
MediaMetadataRetriever._METADATA_KEY_WRITE = android.media.MediaMetadataRetriever.METADATA_KEY_WRITER;
MediaMetadataRetriever._METADATA_KEY_YEAR = android.media.MediaMetadataRetriever.METADATA_KEY_YEAR;
exports.MediaMetadataRetriever = MediaMetadataRetriever;
//# sourceMappingURL=media-metadata-retriever.js.map