"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_media_metadata_retriever_1 = require("nativescript-media-metadata-retriever");
var page_1 = require("ui/page");
var imageSource = require("image-source");
var permissions = require("nativescript-permissions");
var AppComponent = (function () {
    function AppComponent(page) {
        this.page = page;
        this.path = "";
        this.title = "title";
        this.artist = "artist";
        this.allMetadata = "allMetadata";
    }
    AppComponent.prototype.ngOnInit = function () {
        this.mmr = new nativescript_media_metadata_retriever_1.MediaMetadataRetriever(); //Create the object of the classs
        this.page.getViewById("albumArt").set("src", "res://versus_player_logo"); //Set it to a default image if no image is available
        this.getPermissions(); //Requires permission to read internal storage
        console.log("ngOnInit executed...");
    };
    //Get the metadata when the button is pressed
    AppComponent.prototype.getMetadata = function () {
        var _this = this;
        //Set the data source for the media file
        this.mmr.setDataSource(this.path + '');
        //Get a particular metadata
        this.mmr.extractMetadata(nativescript_media_metadata_retriever_1.MediaMetadataRetriever._METADATA_KEY_TITLE) //For title of media
            .then(function (args) {
            _this.title = args;
        });
        this.mmr.extractMetadata(nativescript_media_metadata_retriever_1.MediaMetadataRetriever._METADATA_KEY_ARTIST) //For artist name
            .then(function (args) {
            _this.artist = args;
        });
        //Get all the metadata
        this.mmr.extractAllMetadata()
            .then(function (args) {
            _this.allMetadata = JSON.stringify(args);
        });
        //Get the Embedded Picture(Bitmap)
        this.mmr.getEmbeddedPicture()
            .then(function (args) {
            var albumArt = _this.page.getViewById("albumArt");
            var img = new imageSource.ImageSource();
            img.setNativeSource(args);
            albumArt.set("src", img);
            console.log("ImageSource set...");
        })
            .catch(function (ex) {
            var image = _this.page.getViewById("albumArt");
            image.set("src", "res://versus_player_logo");
            console.log("Failed to set ImageSource..." + ex);
        });
    };
    //This app needs storage permission
    AppComponent.prototype.getPermissions = function () {
        if (!permissions.hasPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            console.log("Asking for permissions...");
            permissions.requestPermissions([
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ])
                .then(function () {
                console.log("Permissions granted...");
            })
                .catch(function () {
                console.log("Permissions denied...");
            });
        }
        else {
            console.log("App has necessary permissions...");
        }
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: "ns-app",
        templateUrl: "app.component.html",
        styleUrls: ["app.component.css"]
    }),
    __metadata("design:paramtypes", [page_1.Page])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsK0ZBQStFO0FBQy9FLGdDQUErQjtBQUcvQiwwQ0FBNEM7QUFFNUMsc0RBQXdEO0FBU3hELElBQWEsWUFBWTtJQVNyQixzQkFBb0IsSUFBVTtRQUFWLFNBQUksR0FBSixJQUFJLENBQU07UUFDMUIsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDZixJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQztRQUNyQixJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQztRQUN2QixJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsQ0FBQztJQUNyQyxDQUFDO0lBRUQsK0JBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSw4REFBc0IsRUFBRSxDQUFDLENBQUksaUNBQWlDO1FBQzdFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsMEJBQTBCLENBQUMsQ0FBQyxDQUFDLG9EQUFvRDtRQUM5SCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBRSw4Q0FBOEM7UUFDdEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFFRCw2Q0FBNkM7SUFDN0Msa0NBQVcsR0FBWDtRQUFBLGlCQWtDQztRQWpDRyx3Q0FBd0M7UUFDeEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsQ0FBQztRQUV2QywyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsOERBQXNCLENBQUMsbUJBQW1CLENBQUMsQ0FBSSxvQkFBb0I7YUFDM0YsSUFBSSxDQUFDLFVBQUMsSUFBSTtZQUNQLEtBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsOERBQXNCLENBQUMsb0JBQW9CLENBQUMsQ0FBRyxpQkFBaUI7YUFDeEYsSUFBSSxDQUFDLFVBQUMsSUFBSTtZQUNQLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLENBQUMsQ0FBQyxDQUFDO1FBRUgsc0JBQXNCO1FBQ3RCLElBQUksQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUU7YUFDNUIsSUFBSSxDQUFDLFVBQUMsSUFBSTtZQUNQLEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QyxDQUFDLENBQUMsQ0FBQztRQUVILGtDQUFrQztRQUNsQyxJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFrQixFQUFFO2FBQzVCLElBQUksQ0FBQyxVQUFDLElBQUk7WUFDUCxJQUFJLFFBQVEsR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNqRCxJQUFJLEdBQUcsR0FBRyxJQUFJLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN4QyxHQUFHLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFCLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN0QyxDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsVUFBQyxFQUFFO1lBQ04sSUFBSSxLQUFLLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztZQUM3QyxPQUFPLENBQUMsR0FBRyxDQUFDLDhCQUE4QixHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ3JELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG1DQUFtQztJQUNuQyxxQ0FBYyxHQUFkO1FBQ0ksRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hGLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUN6QyxXQUFXLENBQUMsa0JBQWtCLENBQUM7Z0JBQzNCLE9BQU8sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLHFCQUFxQjtnQkFDakQsT0FBTyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsc0JBQXNCO2FBQ3JELENBQUM7aUJBQ0QsSUFBSSxDQUFDO2dCQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUMxQyxDQUFDLENBQUM7aUJBQ0QsS0FBSyxDQUFDO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztZQUN6QyxDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0NBQWtDLENBQUMsQ0FBQztRQUNwRCxDQUFDO0lBQ0wsQ0FBQztJQUNMLG1CQUFDO0FBQUQsQ0FBQyxBQTlFRCxJQThFQztBQTlFWSxZQUFZO0lBTnhCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsUUFBUTtRQUNsQixXQUFXLEVBQUUsb0JBQW9CO1FBQ2pDLFNBQVMsRUFBRSxDQUFDLG1CQUFtQixDQUFDO0tBQ25DLENBQUM7cUNBVzRCLFdBQUk7R0FUckIsWUFBWSxDQThFeEI7QUE5RVksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBNZWRpYU1ldGFkYXRhUmV0cmlldmVyIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1tZWRpYS1tZXRhZGF0YS1yZXRyaWV2ZXJcIjtcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuaW1wb3J0IHsgSW1hZ2VGb3JtYXQgfSBmcm9tIFwidWkvZW51bXNcIjtcblxuaW1wb3J0ICogYXMgaW1hZ2VTb3VyY2UgZnJvbSBcImltYWdlLXNvdXJjZVwiO1xuaW1wb3J0ICogYXMgZnMgZnJvbSBcImZpbGUtc3lzdGVtXCI7XG5pbXBvcnQgKiBhcyBwZXJtaXNzaW9ucyBmcm9tIFwibmF0aXZlc2NyaXB0LXBlcm1pc3Npb25zXCI7XG5kZWNsYXJlIHZhciBhbmRyb2lkO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJucy1hcHBcIixcbiAgICB0ZW1wbGF0ZVVybDogXCJhcHAuY29tcG9uZW50Lmh0bWxcIixcbiAgICBzdHlsZVVybHM6IFtcImFwcC5jb21wb25lbnQuY3NzXCJdXG59KVxuXG5leHBvcnQgY2xhc3MgQXBwQ29tcG9uZW50IHsgXG4gICAgcGF0aDogc3RyaW5nO1xuICAgIGFydGlzdDogc3RyaW5nO1xuICAgIHRpdGxlOiBzdHJpbmc7XG4gICAgbW1yOiBNZWRpYU1ldGFkYXRhUmV0cmlldmVyO1xuICAgIGJpdEFycmF5OiBBcnJheTxudW1iZXI+O1xuICAgIGFsbE1ldGFkYXRhOiBzdHJpbmc7XG4gICAgc3JjOiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHBhZ2U6IFBhZ2UpIHtcbiAgICAgICAgdGhpcy5wYXRoID0gXCJcIjtcbiAgICAgICAgdGhpcy50aXRsZSA9IFwidGl0bGVcIjtcbiAgICAgICAgdGhpcy5hcnRpc3QgPSBcImFydGlzdFwiO1xuICAgICAgICB0aGlzLmFsbE1ldGFkYXRhID0gXCJhbGxNZXRhZGF0YVwiO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICB0aGlzLm1tciA9IG5ldyBNZWRpYU1ldGFkYXRhUmV0cmlldmVyKCk7ICAgIC8vQ3JlYXRlIHRoZSBvYmplY3Qgb2YgdGhlIGNsYXNzc1xuICAgICAgICB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJhbGJ1bUFydFwiKS5zZXQoXCJzcmNcIiwgXCJyZXM6Ly92ZXJzdXNfcGxheWVyX2xvZ29cIik7IC8vU2V0IGl0IHRvIGEgZGVmYXVsdCBpbWFnZSBpZiBubyBpbWFnZSBpcyBhdmFpbGFibGVcbiAgICAgICAgdGhpcy5nZXRQZXJtaXNzaW9ucygpOyAgLy9SZXF1aXJlcyBwZXJtaXNzaW9uIHRvIHJlYWQgaW50ZXJuYWwgc3RvcmFnZVxuICAgICAgICBjb25zb2xlLmxvZyhcIm5nT25Jbml0IGV4ZWN1dGVkLi4uXCIpO1xuICAgIH1cblxuICAgIC8vR2V0IHRoZSBtZXRhZGF0YSB3aGVuIHRoZSBidXR0b24gaXMgcHJlc3NlZFxuICAgIGdldE1ldGFkYXRhKCkge1xuICAgICAgICAvL1NldCB0aGUgZGF0YSBzb3VyY2UgZm9yIHRoZSBtZWRpYSBmaWxlXG4gICAgICAgIHRoaXMubW1yLnNldERhdGFTb3VyY2UodGhpcy5wYXRoICsgJycpO1xuXG4gICAgICAgIC8vR2V0IGEgcGFydGljdWxhciBtZXRhZGF0YVxuICAgICAgICB0aGlzLm1tci5leHRyYWN0TWV0YWRhdGEoTWVkaWFNZXRhZGF0YVJldHJpZXZlci5fTUVUQURBVEFfS0VZX1RJVExFKSAgICAvL0ZvciB0aXRsZSBvZiBtZWRpYVxuICAgICAgICAudGhlbigoYXJncykgPT4ge1xuICAgICAgICAgICAgdGhpcy50aXRsZSA9IGFyZ3M7XG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLm1tci5leHRyYWN0TWV0YWRhdGEoTWVkaWFNZXRhZGF0YVJldHJpZXZlci5fTUVUQURBVEFfS0VZX0FSVElTVCkgICAvL0ZvciBhcnRpc3QgbmFtZVxuICAgICAgICAudGhlbigoYXJncykgPT4ge1xuICAgICAgICAgICAgdGhpcy5hcnRpc3QgPSBhcmdzO1xuICAgICAgICB9KTtcblxuICAgICAgICAvL0dldCBhbGwgdGhlIG1ldGFkYXRhXG4gICAgICAgIHRoaXMubW1yLmV4dHJhY3RBbGxNZXRhZGF0YSgpXG4gICAgICAgIC50aGVuKChhcmdzKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmFsbE1ldGFkYXRhID0gSlNPTi5zdHJpbmdpZnkoYXJncyk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vR2V0IHRoZSBFbWJlZGRlZCBQaWN0dXJlKEJpdG1hcClcbiAgICAgICAgdGhpcy5tbXIuZ2V0RW1iZWRkZWRQaWN0dXJlKClcbiAgICAgICAgLnRoZW4oKGFyZ3MpID0+IHtcbiAgICAgICAgICAgIHZhciBhbGJ1bUFydCA9IHRoaXMucGFnZS5nZXRWaWV3QnlJZChcImFsYnVtQXJ0XCIpO1xuICAgICAgICAgICAgdmFyIGltZyA9IG5ldyBpbWFnZVNvdXJjZS5JbWFnZVNvdXJjZSgpO1xuICAgICAgICAgICAgaW1nLnNldE5hdGl2ZVNvdXJjZShhcmdzKTtcbiAgICAgICAgICAgIGFsYnVtQXJ0LnNldChcInNyY1wiLCBpbWcpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJJbWFnZVNvdXJjZSBzZXQuLi5cIik7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaCgoZXgpID0+IHtcbiAgICAgICAgICAgIHZhciBpbWFnZSA9IHRoaXMucGFnZS5nZXRWaWV3QnlJZChcImFsYnVtQXJ0XCIpO1xuICAgICAgICAgICAgaW1hZ2Uuc2V0KFwic3JjXCIsIFwicmVzOi8vdmVyc3VzX3BsYXllcl9sb2dvXCIpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJGYWlsZWQgdG8gc2V0IEltYWdlU291cmNlLi4uXCIgKyBleCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8vVGhpcyBhcHAgbmVlZHMgc3RvcmFnZSBwZXJtaXNzaW9uXG4gICAgZ2V0UGVybWlzc2lvbnMoKTogdm9pZCB7XG4gICAgICAgIGlmICghcGVybWlzc2lvbnMuaGFzUGVybWlzc2lvbihhbmRyb2lkLk1hbmlmZXN0LnBlcm1pc3Npb24uUkVBRF9FWFRFUk5BTF9TVE9SQUdFKSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJBc2tpbmcgZm9yIHBlcm1pc3Npb25zLi4uXCIpO1xuICAgICAgICAgICAgcGVybWlzc2lvbnMucmVxdWVzdFBlcm1pc3Npb25zKFtcbiAgICAgICAgICAgICAgICBhbmRyb2lkLk1hbmlmZXN0LnBlcm1pc3Npb24uUkVBRF9FWFRFUk5BTF9TVE9SQUdFLFxuICAgICAgICAgICAgICAgIGFuZHJvaWQuTWFuaWZlc3QucGVybWlzc2lvbi5XUklURV9FWFRFUk5BTF9TVE9SQUdFXG4gICAgICAgICAgICBdKVxuICAgICAgICAgICAgLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUGVybWlzc2lvbnMgZ3JhbnRlZC4uLlwiKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goKCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUGVybWlzc2lvbnMgZGVuaWVkLi4uXCIpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQXBwIGhhcyBuZWNlc3NhcnkgcGVybWlzc2lvbnMuLi5cIik7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=