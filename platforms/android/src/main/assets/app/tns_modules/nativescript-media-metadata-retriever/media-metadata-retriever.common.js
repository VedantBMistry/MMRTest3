"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("data/observable");
var app = require("application");
var Common = (function (_super) {
    __extends(Common, _super);
    function Common() {
        var _this = _super.call(this) || this;
        _this.message = Utils.SUCCESS_MSG();
        return _this;
    }
    return Common;
}(observable_1.Observable));
exports.Common = Common;
var Utils = (function () {
    function Utils() {
    }
    Utils.SUCCESS_MSG = function () {
        var msg = "[nativescript-media-metadata-retriever]: Plugin is working on " + (app.android ? 'Android' : 'iOS') + ".";
        setTimeout(function () {
            console.log(msg + " Enjoy :p!");
        }, 2000);
        return msg;
    };
    return Utils;
}());
exports.Utils = Utils;
