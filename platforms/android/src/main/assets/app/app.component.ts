import { Component, OnInit } from "@angular/core";
import { MediaMetadataRetriever } from "nativescript-media-metadata-retriever";
import { Page } from "ui/page";
import { ImageFormat } from "ui/enums";

import * as imageSource from "image-source";
import * as fs from "file-system";
import * as permissions from "nativescript-permissions";
declare var android;

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html",
    styleUrls: ["app.component.css"]
})

export class AppComponent { 
    path: string;
    artist: string;
    title: string;
    mmr: MediaMetadataRetriever;
    bitArray: Array<number>;
    allMetadata: string;
    src: string;

    constructor(private page: Page) { }

    ngOnInit(): void {
        this.mmr = new MediaMetadataRetriever();    //Create the object of the classs
        this.page.getViewById("albumArt").set("src", "res://versus_player_logo"); //Set it to a default image if no image is available
        this.getPermissions();  //Requires permission to read internal storage
        console.log("ngOnInit executed...");
    }

    //Get the metadata when the button is pressed
    getMetadata() {
        //Set the data source for the media file
        this.mmr.setDataSource(this.path + '');

        //Get a particular metadata
        this.mmr.extractMetadata(MediaMetadataRetriever._METADATA_KEY_TITLE)    //For title of media
        .then((args) => {
            this.title = args;
        });
        this.mmr.extractMetadata(MediaMetadataRetriever._METADATA_KEY_ARTIST)   //For artist name
        .then((args) => {
            this.artist = args;
        });

        //Get all the metadata
        this.mmr.extractAllMetadata()
        .then((args) => {
            this.allMetadata = JSON.stringify(args);
        });

        //Get the Embedded Picture(Bitmap)
        this.mmr.getEmbeddedPicture()
        .then((args) => {
            var albumArt = this.page.getViewById("albumArt");
            var img = new imageSource.ImageSource();
            img.setNativeSource(args);
            albumArt.set("src", img);
            console.log("ImageSource set...");
        })
        .catch((ex) => {
            var image = this.page.getViewById("albumArt");
            image.set("src", "res://versus_player_logo");
            console.log("Failed to set ImageSource..." + ex);
        });
    }

    //This app needs storage permission
    getPermissions(): void {
        if (!permissions.hasPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            console.log("Asking for permissions...");
            permissions.requestPermissions([
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ])
            .then(() => {
                console.log("Permissions granted...");
            })
            .catch(() => {
                console.log("Permissions denied...");
            })
        } else {
            console.log("App has necessary permissions...");
        }
    }
}
