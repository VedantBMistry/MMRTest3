"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_media_metadata_retriever_1 = require("nativescript-media-metadata-retriever");
var page_1 = require("ui/page");
var imageSource = require("image-source");
var permissions = require("nativescript-permissions");
var AppComponent = (function () {
    function AppComponent(page) {
        this.page = page;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.mmr = new nativescript_media_metadata_retriever_1.MediaMetadataRetriever(); //Create the object of the classs
        this.page.getViewById("albumArt").set("src", "res://versus_player_logo"); //Set it to a default image if no image is available
        this.getPermissions(); //Requires permission to read internal storage
        console.log("ngOnInit executed...");
    };
    //Get the metadata when the button is pressed
    AppComponent.prototype.getMetadata = function () {
        var _this = this;
        //Set the data source for the media file
        this.mmr.setDataSource(this.path + '');
        //Get a particular metadata
        this.mmr.extractMetadata(nativescript_media_metadata_retriever_1.MediaMetadataRetriever._METADATA_KEY_TITLE) //For title of media
            .then(function (args) {
            _this.title = args;
        });
        this.mmr.extractMetadata(nativescript_media_metadata_retriever_1.MediaMetadataRetriever._METADATA_KEY_ARTIST) //For artist name
            .then(function (args) {
            _this.artist = args;
        });
        //Get all the metadata
        this.mmr.extractAllMetadata()
            .then(function (args) {
            _this.allMetadata = JSON.stringify(args);
        });
        //Get the Embedded Picture(Bitmap)
        this.mmr.getEmbeddedPicture()
            .then(function (args) {
            var albumArt = _this.page.getViewById("albumArt");
            var img = new imageSource.ImageSource();
            img.setNativeSource(args);
            albumArt.set("src", img);
            console.log("ImageSource set...");
        })
            .catch(function (ex) {
            var image = _this.page.getViewById("albumArt");
            image.set("src", "res://versus_player_logo");
            console.log("Failed to set ImageSource..." + ex);
        });
    };
    //This app needs storage permission
    AppComponent.prototype.getPermissions = function () {
        if (!permissions.hasPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            console.log("Asking for permissions...");
            permissions.requestPermissions([
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ])
                .then(function () {
                console.log("Permissions granted...");
            })
                .catch(function () {
                console.log("Permissions denied...");
            });
        }
        else {
            console.log("App has necessary permissions...");
        }
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: "ns-app",
        templateUrl: "app.component.html",
        styleUrls: ["app.component.css"]
    }),
    __metadata("design:paramtypes", [page_1.Page])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsK0ZBQStFO0FBQy9FLGdDQUErQjtBQUcvQiwwQ0FBNEM7QUFFNUMsc0RBQXdEO0FBU3hELElBQWEsWUFBWTtJQVNyQixzQkFBb0IsSUFBVTtRQUFWLFNBQUksR0FBSixJQUFJLENBQU07SUFBSSxDQUFDO0lBRW5DLCtCQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksOERBQXNCLEVBQUUsQ0FBQyxDQUFJLGlDQUFpQztRQUM3RSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLDBCQUEwQixDQUFDLENBQUMsQ0FBQyxvREFBb0Q7UUFDOUgsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUUsOENBQThDO1FBQ3RFLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQsNkNBQTZDO0lBQzdDLGtDQUFXLEdBQVg7UUFBQSxpQkFrQ0M7UUFqQ0csd0NBQXdDO1FBQ3hDLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFFdkMsMkJBQTJCO1FBQzNCLElBQUksQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLDhEQUFzQixDQUFDLG1CQUFtQixDQUFDLENBQUksb0JBQW9CO2FBQzNGLElBQUksQ0FBQyxVQUFDLElBQUk7WUFDUCxLQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUN0QixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLDhEQUFzQixDQUFDLG9CQUFvQixDQUFDLENBQUcsaUJBQWlCO2FBQ3hGLElBQUksQ0FBQyxVQUFDLElBQUk7WUFDUCxLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN2QixDQUFDLENBQUMsQ0FBQztRQUVILHNCQUFzQjtRQUN0QixJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFrQixFQUFFO2FBQzVCLElBQUksQ0FBQyxVQUFDLElBQUk7WUFDUCxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUMsQ0FBQyxDQUFDLENBQUM7UUFFSCxrQ0FBa0M7UUFDbEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsRUFBRTthQUM1QixJQUFJLENBQUMsVUFBQyxJQUFJO1lBQ1AsSUFBSSxRQUFRLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDakQsSUFBSSxHQUFHLEdBQUcsSUFBSSxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDeEMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixRQUFRLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLFVBQUMsRUFBRTtZQUNOLElBQUksS0FBSyxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzlDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLDBCQUEwQixDQUFDLENBQUM7WUFDN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUNyRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtQ0FBbUM7SUFDbkMscUNBQWMsR0FBZDtRQUNJLEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoRixPQUFPLENBQUMsR0FBRyxDQUFDLDJCQUEyQixDQUFDLENBQUM7WUFDekMsV0FBVyxDQUFDLGtCQUFrQixDQUFDO2dCQUMzQixPQUFPLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUI7Z0JBQ2pELE9BQU8sQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLHNCQUFzQjthQUNyRCxDQUFDO2lCQUNELElBQUksQ0FBQztnQkFDRixPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDMUMsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQztnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7WUFDekMsQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7UUFDcEQsQ0FBQztJQUNMLENBQUM7SUFDTCxtQkFBQztBQUFELENBQUMsQUF6RUQsSUF5RUM7QUF6RVksWUFBWTtJQU54QixnQkFBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLFFBQVE7UUFDbEIsV0FBVyxFQUFFLG9CQUFvQjtRQUNqQyxTQUFTLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztLQUNuQyxDQUFDO3FDQVc0QixXQUFJO0dBVHJCLFlBQVksQ0F5RXhCO0FBekVZLG9DQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgTWVkaWFNZXRhZGF0YVJldHJpZXZlciB9IGZyb20gXCJuYXRpdmVzY3JpcHQtbWVkaWEtbWV0YWRhdGEtcmV0cmlldmVyXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcbmltcG9ydCB7IEltYWdlRm9ybWF0IH0gZnJvbSBcInVpL2VudW1zXCI7XG5cbmltcG9ydCAqIGFzIGltYWdlU291cmNlIGZyb20gXCJpbWFnZS1zb3VyY2VcIjtcbmltcG9ydCAqIGFzIGZzIGZyb20gXCJmaWxlLXN5c3RlbVwiO1xuaW1wb3J0ICogYXMgcGVybWlzc2lvbnMgZnJvbSBcIm5hdGl2ZXNjcmlwdC1wZXJtaXNzaW9uc1wiO1xuZGVjbGFyZSB2YXIgYW5kcm9pZDtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwibnMtYXBwXCIsXG4gICAgdGVtcGxhdGVVcmw6IFwiYXBwLmNvbXBvbmVudC5odG1sXCIsXG4gICAgc3R5bGVVcmxzOiBbXCJhcHAuY29tcG9uZW50LmNzc1wiXVxufSlcblxuZXhwb3J0IGNsYXNzIEFwcENvbXBvbmVudCB7IFxuICAgIHBhdGg6IHN0cmluZztcbiAgICBhcnRpc3Q6IHN0cmluZztcbiAgICB0aXRsZTogc3RyaW5nO1xuICAgIG1tcjogTWVkaWFNZXRhZGF0YVJldHJpZXZlcjtcbiAgICBiaXRBcnJheTogQXJyYXk8bnVtYmVyPjtcbiAgICBhbGxNZXRhZGF0YTogc3RyaW5nO1xuICAgIHNyYzogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlOiBQYWdlKSB7IH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICB0aGlzLm1tciA9IG5ldyBNZWRpYU1ldGFkYXRhUmV0cmlldmVyKCk7ICAgIC8vQ3JlYXRlIHRoZSBvYmplY3Qgb2YgdGhlIGNsYXNzc1xuICAgICAgICB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXCJhbGJ1bUFydFwiKS5zZXQoXCJzcmNcIiwgXCJyZXM6Ly92ZXJzdXNfcGxheWVyX2xvZ29cIik7IC8vU2V0IGl0IHRvIGEgZGVmYXVsdCBpbWFnZSBpZiBubyBpbWFnZSBpcyBhdmFpbGFibGVcbiAgICAgICAgdGhpcy5nZXRQZXJtaXNzaW9ucygpOyAgLy9SZXF1aXJlcyBwZXJtaXNzaW9uIHRvIHJlYWQgaW50ZXJuYWwgc3RvcmFnZVxuICAgICAgICBjb25zb2xlLmxvZyhcIm5nT25Jbml0IGV4ZWN1dGVkLi4uXCIpO1xuICAgIH1cblxuICAgIC8vR2V0IHRoZSBtZXRhZGF0YSB3aGVuIHRoZSBidXR0b24gaXMgcHJlc3NlZFxuICAgIGdldE1ldGFkYXRhKCkge1xuICAgICAgICAvL1NldCB0aGUgZGF0YSBzb3VyY2UgZm9yIHRoZSBtZWRpYSBmaWxlXG4gICAgICAgIHRoaXMubW1yLnNldERhdGFTb3VyY2UodGhpcy5wYXRoICsgJycpO1xuXG4gICAgICAgIC8vR2V0IGEgcGFydGljdWxhciBtZXRhZGF0YVxuICAgICAgICB0aGlzLm1tci5leHRyYWN0TWV0YWRhdGEoTWVkaWFNZXRhZGF0YVJldHJpZXZlci5fTUVUQURBVEFfS0VZX1RJVExFKSAgICAvL0ZvciB0aXRsZSBvZiBtZWRpYVxuICAgICAgICAudGhlbigoYXJncykgPT4ge1xuICAgICAgICAgICAgdGhpcy50aXRsZSA9IGFyZ3M7XG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLm1tci5leHRyYWN0TWV0YWRhdGEoTWVkaWFNZXRhZGF0YVJldHJpZXZlci5fTUVUQURBVEFfS0VZX0FSVElTVCkgICAvL0ZvciBhcnRpc3QgbmFtZVxuICAgICAgICAudGhlbigoYXJncykgPT4ge1xuICAgICAgICAgICAgdGhpcy5hcnRpc3QgPSBhcmdzO1xuICAgICAgICB9KTtcblxuICAgICAgICAvL0dldCBhbGwgdGhlIG1ldGFkYXRhXG4gICAgICAgIHRoaXMubW1yLmV4dHJhY3RBbGxNZXRhZGF0YSgpXG4gICAgICAgIC50aGVuKChhcmdzKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmFsbE1ldGFkYXRhID0gSlNPTi5zdHJpbmdpZnkoYXJncyk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vR2V0IHRoZSBFbWJlZGRlZCBQaWN0dXJlKEJpdG1hcClcbiAgICAgICAgdGhpcy5tbXIuZ2V0RW1iZWRkZWRQaWN0dXJlKClcbiAgICAgICAgLnRoZW4oKGFyZ3MpID0+IHtcbiAgICAgICAgICAgIHZhciBhbGJ1bUFydCA9IHRoaXMucGFnZS5nZXRWaWV3QnlJZChcImFsYnVtQXJ0XCIpO1xuICAgICAgICAgICAgdmFyIGltZyA9IG5ldyBpbWFnZVNvdXJjZS5JbWFnZVNvdXJjZSgpO1xuICAgICAgICAgICAgaW1nLnNldE5hdGl2ZVNvdXJjZShhcmdzKTtcbiAgICAgICAgICAgIGFsYnVtQXJ0LnNldChcInNyY1wiLCBpbWcpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJJbWFnZVNvdXJjZSBzZXQuLi5cIik7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaCgoZXgpID0+IHtcbiAgICAgICAgICAgIHZhciBpbWFnZSA9IHRoaXMucGFnZS5nZXRWaWV3QnlJZChcImFsYnVtQXJ0XCIpO1xuICAgICAgICAgICAgaW1hZ2Uuc2V0KFwic3JjXCIsIFwicmVzOi8vdmVyc3VzX3BsYXllcl9sb2dvXCIpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJGYWlsZWQgdG8gc2V0IEltYWdlU291cmNlLi4uXCIgKyBleCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8vVGhpcyBhcHAgbmVlZHMgc3RvcmFnZSBwZXJtaXNzaW9uXG4gICAgZ2V0UGVybWlzc2lvbnMoKTogdm9pZCB7XG4gICAgICAgIGlmICghcGVybWlzc2lvbnMuaGFzUGVybWlzc2lvbihhbmRyb2lkLk1hbmlmZXN0LnBlcm1pc3Npb24uUkVBRF9FWFRFUk5BTF9TVE9SQUdFKSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJBc2tpbmcgZm9yIHBlcm1pc3Npb25zLi4uXCIpO1xuICAgICAgICAgICAgcGVybWlzc2lvbnMucmVxdWVzdFBlcm1pc3Npb25zKFtcbiAgICAgICAgICAgICAgICBhbmRyb2lkLk1hbmlmZXN0LnBlcm1pc3Npb24uUkVBRF9FWFRFUk5BTF9TVE9SQUdFLFxuICAgICAgICAgICAgICAgIGFuZHJvaWQuTWFuaWZlc3QucGVybWlzc2lvbi5XUklURV9FWFRFUk5BTF9TVE9SQUdFXG4gICAgICAgICAgICBdKVxuICAgICAgICAgICAgLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUGVybWlzc2lvbnMgZ3JhbnRlZC4uLlwiKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goKCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUGVybWlzc2lvbnMgZGVuaWVkLi4uXCIpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQXBwIGhhcyBuZWNlc3NhcnkgcGVybWlzc2lvbnMuLi5cIik7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=